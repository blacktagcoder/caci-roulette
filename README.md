# README #


### What is this repository for? ###

* Tech test for CACI 


### How do I get set up? ###

* A basic roulette application using springboot and gradle 
* commands are as follows 
* ./gradlew clean
* ./gradlew test
* ./gradlew build   or ./gradlew assemble
* 
* to run the app either run ./gradlew bootrun from the project directory i.e. containing the build.gradle  
* or run the jar contained in the build directory    java -jar  com.sep.caci-roulette-0.1.0.jar

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner Sep Russell
* Sep.russell@hotmail.co.uk