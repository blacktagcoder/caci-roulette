package com.sep.caci.roulette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Random;
import java.util.stream.IntStream;

import static com.sep.caci.roulette.RouletteWheel.POCKETS;
import static java.lang.System.out;

@SpringBootApplication
public class RouletteApplication {

	public static void main(String[] args) {
		
		ApplicationContext context = SpringApplication.run(RouletteApplication.class, args);
		RouletteApplication draftRouletteApplication = new RouletteApplication();
		draftRouletteApplication.runRoulette((RouletteWheel) context.getBean("rouletteWheel"));
	}

	private void runRoulette(RouletteWheel rouletteWheel) {
		out.println("\n +++++++++++++++++++++++++++++++++++++++++++++++++++++ run started +++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");

		// example run
		Customer customer = new Customer("first", "last", 0);

//		Bet.BetCriteria selectedBetType = getRandomSelectedType();

		IntStream.range(0,POCKETS.size())
				.forEach(i->{
					Bet bet = new Bet(customer.getWinnings(), 10, getRandomSelectedType());
					bet.setPlacedOn(new Pocket(i,false));//needed for pocket bet types

					try {
						//reset
						customer.getWinnings().getValue().getAndSet(0);
//						out.format("\n initial  winnings are : %d -",customer.getWinningsAmount());
//						out.println("\n please place a bet :- ");


						Pocket winningPocket= rouletteWheel.spin(bet);

						out.format("\n winning pocket is %s",winningPocket.getNumber());
						out.format("\n your bet was type %s and value of %s ",bet.getBetType(), bet.getBetType()==Bet.BetCriteria.pocket?i:bet.getBetType());
						out.format("\n your winnings is %d\n\n", customer.getWinningsAmount());

					} catch (RouletteGameException e) {
						e.printStackTrace();
					}

				});
		out.println("\n +++++++++++++++++++++++++++++++++++++++++++++++++++++ run complete +++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");
	}

	private Bet.BetCriteria getRandomSelectedType(){
		Bet.BetCriteria[] betValues = Bet.BetCriteria.values();
		return betValues[new Random().nextInt(betValues.length)];
	}
	@Bean
	public RouletteWheel rouletteWheel(){
		return new RouletteWheel();
	}

}
