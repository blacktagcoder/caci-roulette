package com.sep.caci.roulette;

public class Bet {
    private final Winnings winnings;
    private final int betAmount;

    public BetCriteria getBetType() {
        return betType;
    }

    public enum BetCriteria {
        odds, evens, pocket
    }

    private final BetCriteria betType;

    public Pocket getPlacedOn() {
        return placedOn;
    }

    public void setPlacedOn(Pocket placedOn) {
        this.placedOn = placedOn;
    }

    private Pocket placedOn=null;

    public Winnings getWinnings(){
        return this.winnings;
    }
    public  Customer getCustomer(){
        return this.winnings.getCustomer();
    }

    public int getBetAmount() {
        return betAmount;
    }

    public Bet(Winnings winnings, int betAmount, BetCriteria betCriteria) {
        this(winnings, betAmount, betCriteria, new Pocket(0, false));
    }
    public Bet(Winnings winnings, int betAmount, BetCriteria betCriteria, Pocket pocket) {
        this.winnings = winnings;
        this.betAmount = betAmount;
        this.betType = betCriteria;
        this.placedOn = pocket;
    }
}
