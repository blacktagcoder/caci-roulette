package com.sep.caci.roulette;

public class RouletteGameException extends Exception{
    public RouletteGameException(String message) {
        super(message);
    }
}
