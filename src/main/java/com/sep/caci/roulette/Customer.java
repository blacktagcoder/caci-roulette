package com.sep.caci.roulette;

public class Customer {
    private final String firstName;
    private final String secondName;
    private final Winnings winnings;

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public Winnings getWinnings() {
        return winnings;
    }
    public int getWinningsAmount() {
        return winnings.getValue().get();
    }

    public Customer(String firstName, String secondName, int initialAccountValue) {

        this.firstName = firstName;
        this.secondName = secondName;
        this.winnings =  new Winnings(this, initialAccountValue);
    }

}
