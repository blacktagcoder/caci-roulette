package com.sep.caci.roulette;

public class Pocket {

    private int number=0;
    private boolean occupied =false;
    private boolean invalid;

    public int getNumber() {
        return number;
    }

    public Pocket(int number, boolean occupied) {
        this(number,occupied,false);
    }

    public Pocket(int number, boolean occupied, boolean invalid) {
        this.number = number;
        this.occupied = occupied;
        this.invalid = invalid;
    }

    public boolean isInvalid() {
        return invalid;
    }
    public boolean isOccupied() {
        return occupied;
    }
}
