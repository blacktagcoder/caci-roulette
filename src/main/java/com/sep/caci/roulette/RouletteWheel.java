package com.sep.caci.roulette;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Comparator.comparing;

public class RouletteWheel {

    public static final String INVALID_BET_AMOUNT_BET_SHOULD_BE_GREATER_THAN_ZERO = "Invalid bet amount, bet should be greater than zero";
    public static final String INVALID_POCKET_SELECTED = "INVALID_POCKET_SELECTED";
    private  final Random random = new Random();
    //roulette wheel / pockets
    public static final Map<Integer, Pocket> POCKETS = new HashMap<>();
    public static final Map<Integer, Pocket> ODDS = new HashMap<>();
    public static final Map<Integer, Pocket> EVENS = new HashMap<>();
    //pockets of the wheel initialise
    static {

        //wheel values
        IntStream.rangeClosed(0, 36)
                .forEach(i ->
                        POCKETS.put(i,
                                new Pocket(i, false))
                );

        ODDS.putAll(POCKETS.entrySet()
                .stream()
                .filter(RouletteWheel::oddsPredicate)
                .sorted(comparing(i -> i.getKey()))
                .collect(Collectors.toMap(q -> q.getKey(), q -> q.getValue())));
        ODDS.remove(0);

        EVENS.putAll(POCKETS.entrySet()
                .stream()
                .filter(RouletteWheel::evensPredicate)
                .sorted(comparing(i -> i.getKey()))
                .collect(Collectors.toMap(q -> q.getKey(), q -> q.getValue())));
        EVENS.remove(0);
    }
    public Pocket spin(Bet bet) throws RouletteGameException {
        return this.spin(bet, (b) -> POCKETS.get(random.nextInt(POCKETS.size())));
    }

    public Pocket spin(final Bet bet, final Function<Object, Pocket> spinner) throws RouletteGameException {
        //validation
        if (bet.getBetAmount() <= 0) {
            throw new RouletteGameException(INVALID_BET_AMOUNT_BET_SHOULD_BE_GREATER_THAN_ZERO);
        }
        Pocket p = spinner.apply(null);
        if (p.isInvalid()|| !POCKETS.containsKey(bet.getPlacedOn().getNumber())) {
            throw new RouletteGameException(INVALID_POCKET_SELECTED);
        }

        final Winnings winnings = bet.getWinnings();

        // winning scenarios adds winnings  while  losing scenarios do nothing!
        switch (bet.getBetType()) {
            case evens: {
                EVENS.remove(0);
                if (EVENS.containsKey(p.getNumber())) {
                    winnings
                            .getValue()
                            .updateAndGet(w -> w + 20);
                }
                break;
            }
            case odds: {
                ODDS.remove(0);
                if (ODDS.containsKey(p.getNumber())) {
                    winnings
                            .getValue()
                            .updateAndGet(i -> i + 20);
                }
                break;
            }
            case pocket: {
                if (p.getNumber() == bet.getPlacedOn().getNumber()) {
                    winnings.getValue().updateAndGet(i -> i + 360);
                }
                break;
            }
            //
            default:
        }
        return p;
    }
    private  static boolean evensPredicate(  Map.Entry<Integer, Pocket> entry) {
        return entry.getKey() % 2 == 0;

    }
    private static boolean oddsPredicate(  Map.Entry<Integer, Pocket> entry) {
        return entry.getKey() % 2 != 0;
    }
}
