package com.sep.caci.roulette;

import java.util.concurrent.atomic.AtomicInteger;

public class Winnings {
    private final AtomicInteger value= new AtomicInteger(0);
    private Customer customer;


    public Winnings(Customer customer, int value) {
        this.customer = customer;
        this.value.set(value);
    }

    public void setValue(int value) {
        this.value.set(value);
    }

    public Customer getCustomer() {
        return customer;
    }

    public AtomicInteger getValue() {

        return value;

    }
}
