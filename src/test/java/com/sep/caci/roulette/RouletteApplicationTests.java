package com.sep.caci.roulette;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.sep.caci.roulette.RouletteWheel.POCKETS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RouletteApplicationTests {

	public static final String INVALID_BET_AMOUNT_BET_SHOULD_BE_GREATER_THAN_ZERO = "Invalid bet amount, bet should be greater than zero";
	public static final String INVALID_POCKET_SELECTED = "INVALID_POCKET_SELECTED";

	@Test
	public void contextLoads() {
	}


	//	Stage 1 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//
	@Test
	public void givenACustomer_placesBetOnLosingPocket_receives0Winnings() throws Exception {

		//Given
		int initialAccountValue = 10;
		int betAmount=initialAccountValue;
		String firstName = "firstname";
		String secondName = "secondname";
		RouletteTest rouletteTest = getRouletteTest(initialAccountValue, betAmount, firstName, secondName, Bet.BetCriteria.pocket);

		Customer  customer =  rouletteTest.getCustomer();
		Bet bet =  rouletteTest.getBet();
		Pocket placedOn = new Pocket(1, false);
		Pocket winningPocket = new Pocket(0, false);

		bet.setPlacedOn(placedOn);
		RouletteWheel rouletteWheel =  rouletteTest.getRouletteWheel();


		//when
		rouletteWheel.spin(bet, (b) -> winningPocket);


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(initialAccountValue);
		assertThat(bet.getPlacedOn().isOccupied()).isEqualTo(false);
	}

	//candidate domain objects Customer, roulettewheel, bet , winnings, pocket, ball
	@Test
	public void givenACustomer_placesBetOnLosingPocket_receives0Winnings2() throws Exception {
		int initialAccountValue = 20;

		RouletteTest rouletteTest = getRouletteTest(initialAccountValue, 10, "firstName", "secondName",Bet.BetCriteria.pocket);
		Customer  customer =  rouletteTest.getCustomer();
		Bet bet =  rouletteTest.getBet();
		Pocket placedOn = new Pocket(1, false);
		Pocket winningPocket = new Pocket(0, false);
		bet.setPlacedOn(placedOn);
		RouletteWheel rouletteWheel =  rouletteTest.getRouletteWheel();


		rouletteWheel.spin(bet, (b)-> winningPocket);


		assertThat(customer.getWinnings().getValue().get()).isEqualTo(initialAccountValue);// assumed winnings simple added to account
		assertThat(bet.getPlacedOn().isOccupied()).isEqualTo(false);
	}


	@Test
	public void givenACustomer_placesBetOnWinningPocket_receives360Winnings() throws Exception {

		//given
		int initialAccountValue = 50;
		int betAmount = 10;
		RouletteTest rouletteTest = getRouletteTest(50, betAmount, "firstName", "secondName", Bet.BetCriteria.pocket);
		Customer  customer =  rouletteTest.getCustomer();
		Bet bet =  rouletteTest.getBet();
		bet.setPlacedOn(new Pocket(0, true));
		RouletteWheel rouletteWheel =  rouletteTest.getRouletteWheel();


		//when
		Pocket pocket =  rouletteWheel.spin(bet, (b)-> new Pocket(0, true) );


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(360+initialAccountValue);
		assertThat(pocket.isOccupied()).isEqualTo(true);


	}
	//	Stage 2 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//
	@Test
	public void CustomerPlacesInvalidZeroBet_throwsException() throws Exception {
		RouletteTest rouletteTest = getRouletteTest(10, 0, "firstName", "secondName", Bet.BetCriteria.pocket);
		Customer  customer =  rouletteTest.getCustomer();
		Bet bet =  rouletteTest.getBet();
		RouletteWheel rouletteWheel =  rouletteTest.getRouletteWheel();

		Throwable throwable = catchThrowable(()->{rouletteWheel.spin(bet, (b)-> new Pocket(0, true) );});


		assertThat(throwable)
				.isInstanceOf(RouletteGameException.class)
				.hasMessage(INVALID_BET_AMOUNT_BET_SHOULD_BE_GREATER_THAN_ZERO);
	}

	@Test
	public void CustomerPlacesInvalidLessThanZeroBet_throwsException2() throws Exception {
		RouletteTest rouletteTest = getRouletteTest(10, -1, "firstName", "secondName", Bet.BetCriteria.pocket);
		//Customer  customer =  rouletteTest.getCustomer();
		Bet bet =  rouletteTest.getBet();
		RouletteWheel rouletteWheel =  rouletteTest.getRouletteWheel();

		Throwable throwable = catchThrowable(()->{rouletteWheel.spin(bet, (b)-> new Pocket(0, true) );});


		assertThat(throwable)
				.isInstanceOf(RouletteGameException.class)
				.hasMessage(INVALID_BET_AMOUNT_BET_SHOULD_BE_GREATER_THAN_ZERO);
	}

	@Test
	public void CustomerPlacesInvalidPocket_throwsException2() throws Exception {
		RouletteTest rouletteTest = getRouletteTest(10, 10, "firstName", "secondName", Bet.BetCriteria.pocket);
		//Customer  customer =  rouletteTest.getCustomer();
		Bet bet =  rouletteTest.getBet();
		RouletteWheel rouletteWheel =  rouletteTest.getRouletteWheel();

		Throwable throwable = catchThrowable(()->{rouletteWheel.spin(bet, (b)-> new Pocket(0,true,true) );});


		assertThat(throwable)
				.isNotNull()
				.isInstanceOf(RouletteGameException.class)
				.hasMessage(INVALID_POCKET_SELECTED);
	}

//	Stage 3 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//

	@Test
	public void CustomerPlacesOddsBetBallLandsOnZero_customerReceives0Winnings() throws Exception {
		//given
		int betAmount = 10;
		RouletteTest rouletteTest = getRouletteTest(10, betAmount, "firstName", "secondName", Bet.BetCriteria.odds);
		Customer customer = rouletteTest.getCustomer();
		Bet bet = rouletteTest.getBet();
		RouletteWheel rouletteWheel = rouletteTest.getRouletteWheel();


		//arrange
		rouletteWheel.spin(bet, (b) -> POCKETS.get(0));


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(10);
	}
	@Test
	public void CustomerPlacesEvensBetBallLandsOnZero_customerReceives0Winnings() throws Exception {
		//given
		int betAmount = 10;
		RouletteTest rouletteTest = getRouletteTest(10, betAmount, "firstName", "secondName", Bet.BetCriteria.evens);
		Customer customer = rouletteTest.getCustomer();
		Bet bet = rouletteTest.getBet();
		RouletteWheel rouletteWheel = rouletteTest.getRouletteWheel();


		//arrange
		rouletteWheel.spin(bet, (b) -> POCKETS.get(0));


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(10);
	}
	@Test
	public void CustomerPlacesEvensBetBallLandsOnEvens_customerReceives20Winnings() throws Exception {
		//given
		int betAmount = 10;
		RouletteTest rouletteTest = getRouletteTest(10, betAmount, "firstName", "secondName", Bet.BetCriteria.evens);
		Customer customer = rouletteTest.getCustomer();
		Bet bet = rouletteTest.getBet();
		RouletteWheel rouletteWheel = rouletteTest.getRouletteWheel();


		//arrange
		rouletteWheel.spin(bet, (b) -> POCKETS.get(2));


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(30);
	}
	@Test
	public void CustomerPlacesEvensBetBallLandsOnEvens_customerReceives0Winnings() throws Exception {
		//given
		int betAmount = 10;
		RouletteTest rouletteTest = getRouletteTest(10, betAmount, "firstName", "secondName", Bet.BetCriteria.evens);
		Customer customer = rouletteTest.getCustomer();
		Bet bet = rouletteTest.getBet();
		RouletteWheel rouletteWheel = rouletteTest.getRouletteWheel();


		//arrange
		rouletteWheel.spin(bet, (b) -> POCKETS.get(1));


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(10);//just the initial winnings
	}
	@Test
	public void CustomerPlacesOddsBetBallLandsOnEvens_customerReceives0Winnings() throws Exception {
		//given
		int betAmount = 10;
		RouletteTest rouletteTest = getRouletteTest(10, betAmount, "firstName", "secondName", Bet.BetCriteria.odds);
		Customer customer = rouletteTest.getCustomer();
		Bet bet = rouletteTest.getBet();
		RouletteWheel rouletteWheel = rouletteTest.getRouletteWheel();


		//arrange
		rouletteWheel.spin(bet, (b) -> POCKETS.get(4));


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(10);//just the initial winnings
	}

	@Test
	public void CustomerPlacesOddsBetBallLandsOnOdds_customerReceives20Winnings() throws Exception {
		//given
		int betAmount = 15;
		RouletteTest rouletteTest = getRouletteTest(10, betAmount, "firstName", "secondName", Bet.BetCriteria.odds);
		Customer customer = rouletteTest.getCustomer();
		Bet bet = rouletteTest.getBet();
		RouletteWheel rouletteWheel = rouletteTest.getRouletteWheel();


		//arrange
		rouletteWheel.spin(bet, (b) -> POCKETS.get(5));


		//assert
		assertThat(customer.getWinnings().getValue().get()).isEqualTo(30);//just the initial and new winnings
	}
	@Test
	public void spinWheel_returnRandomPocket() throws Exception {

		RouletteTest rouletteTest = getRouletteTest(10, 10, "firstName", "secondName", Bet.BetCriteria.pocket);
		Bet bet = rouletteTest.getBet();
		bet.setPlacedOn(new Pocket(1,false));


		Pocket pocket = rouletteTest.getRouletteWheel().spin(bet);

		assertThat(pocket).isNotNull();

	}



	//helpers for tests
	private RouletteTest getRouletteTest(int initialAccountValue, int betAmount, String firstName, String secondName, Bet.BetCriteria betCriteria) {
		return new RouletteTestBuilder()
				.configureCustomer(firstName, secondName, initialAccountValue)
				.configureBet(betAmount,betCriteria)
				.configureRouletteWheel()
				.build();
	}
	public static class RouletteTestBuilder {

		private Customer customer;
		private RouletteWheel rouletteWheel;
		private Bet bet;
		private String firstname;
		private String secondname;
		private int winnings;
		private int betAmount;
		private Bet.BetCriteria betCriteria;

		public RouletteTestBuilder configureCustomer(Customer customer){
			this.customer = customer;
			return this;
		}
		public RouletteTestBuilder configureRouletteWheel(){
			this.rouletteWheel = new RouletteWheel();
			return  this;
		}
		public RouletteTestBuilder configureBet(Bet bet, Bet.BetCriteria betCriteria){
			this.bet = bet;
			this.betCriteria = betCriteria;
			return this;}


		public RouletteTest build(){

			this.customer = new Customer(firstname, secondname, winnings);
			return new RouletteTest( customer,new Bet(customer.getWinnings(), betAmount, betCriteria),new RouletteWheel());
		}


		public RouletteTestBuilder configureCustomer(String firstname, String secondname, int initialAccountValue) {
			this.firstname = firstname;
			this.secondname = secondname;
			this.winnings = initialAccountValue;

			return  this;
		}

		public RouletteTestBuilder configureBet(int betAmount, Bet.BetCriteria betCriteria) {
			this.betAmount = betAmount;
			this.betCriteria = betCriteria;
			return this;
		}
	}
	public static class RouletteTest {

		private final Customer customer;
		private final Bet bet;
		private final RouletteWheel rouletteWheel;

		public Customer getCustomer() {
			return customer;
		}

		public Bet getBet() {
			return bet;
		}

		public RouletteWheel getRouletteWheel() {
			return rouletteWheel;
		}

		public RouletteTest(Customer customer, Bet bet, RouletteWheel rouletteWheel) {

			this.customer = customer;
			this.bet = bet;
			this.rouletteWheel = rouletteWheel;
		}
	}

}
